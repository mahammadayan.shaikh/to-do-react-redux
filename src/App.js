import { Provider } from "react-redux"
import AddToDo from "./components/AddToDo";
import ToDoItemList from "./components/ToDoItemList";
import store from "./store";

function App() {
  return (
    <Provider store={store}>
      <AddToDo />
      <ToDoItemList />
    </Provider>
  );
}

export default App;
