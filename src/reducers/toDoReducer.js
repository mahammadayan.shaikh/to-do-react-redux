const initialState = {
    toDoItems: []
}

const toDoReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return {
                ...state,
                toDoItems: [...state.toDoItems, action.payload]
            }

        case 'DELETE_TODOS':
            return {
                ...state,
                toDoItems: state.toDoItems.filter(item => item.id !== action.payload.id)
            }

        default:
            return state
    }
}

export default toDoReducer;