import React from 'react'
import { connect } from 'react-redux'
import ToDoItem from './ToDoItem'

function ToDoItemList(props) {
    return (
        <div>
            {
                props.toDoItems &&
                props.toDoItems.map(toDoItem => {
                    return <ToDoItem item={toDoItem} key={toDoItem.id} />
                })
            }
        </div>
    )
}

const mapStateToProps = state => {
    return {
        toDoItems: state.todo.toDoItems
    }
}

export default connect(mapStateToProps)(ToDoItemList)
