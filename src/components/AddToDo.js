import React, { useState } from 'react'
import { connect } from 'react-redux'

import { addToDo } from '../actions'

const AddToDo = (props) => {
    const [name, setName] = useState('')
    return (
        <div>
            <input type="text" value={name} onChange={e => setName(e.target.value)} placeholder="Enter To Do Item" /> <br /><br />
            <button onClick={() => props.addToDo(name)}>Add</button>
        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        addToDo: toDoItemName => dispatch(addToDo(toDoItemName))
    }
}

export default connect(null, mapDispatchToProps)(AddToDo)
