import React from 'react'
import { connect } from 'react-redux'
import { deleteToDo } from '../actions'

function ToDoItem({ item, deleteToDo }) {
    return (
        <div>
            <h2>{item.name}</h2>
            <button onClick={() => deleteToDo(item.id)}>Delete</button>
            <hr />
        </div>
    )
}
const mapDispatchToProps = dispatch => {
    return {
        deleteToDo: id => dispatch(deleteToDo(id))
    }
}

export default connect(null, mapDispatchToProps)(ToDoItem)
