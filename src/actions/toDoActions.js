var id = 0;

export const addToDo = (toDoItemName) => {
    return {
        type: 'ADD_TODO',
        payload: {
            id: ++id,
            name: toDoItemName
        }
    }
}

export const deleteToDo = (id) => {
    return {
        type: 'DELETE_TODOS',
        payload: {
            id
        }
    }
}